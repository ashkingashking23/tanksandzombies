using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlay_controller : MonoBehaviour
{
    public static GamePlay_controller instance = null;

    [SerializeField] GameObject[] obstacleObjects, zombieObjects;
    [SerializeField] Transform[] lanes;
    [SerializeField] float min_delay = 10f, max_delay = 30f;
    float halfGroundSize;
    BaseController baseController = null;

    private void Awake()
    {
        Inititalizer();   
    }

    void Inititalizer()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }
}
