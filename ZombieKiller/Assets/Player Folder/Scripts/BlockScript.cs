using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockScript : MonoBehaviour
{
    [SerializeField] Transform otherBlock;
    Transform Player;
    [SerializeField] float endOffset = 10f, halflength =100f;

    private void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        Moveground(); 
    }

    void Moveground()
    {
        if (transform.position.z + halflength < Player.position.z - endOffset)
        {
            transform.position = new Vector3(otherBlock.position.x, otherBlock.position.y, otherBlock.position.z + halflength * 2);
        }
    }
}
