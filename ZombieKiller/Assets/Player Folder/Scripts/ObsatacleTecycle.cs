using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsatacleTecycle : MonoBehaviour
{
    GameObject tank = null;

    private void Start() => tank = FindObjectOfType<PlayerController>().gameObject;


    private void Update()
    {
        if (gameObject.activeSelf && (gameObject.transform.position.z + 5f < tank.transform.position.z))
        {
            PoolManager._instance.RecycleObstacles(gameObject);
        }
    }
}
