using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieRecylce : MonoBehaviour
{
    GameObject tank = null;
    private void Start() =>tank = FindObjectOfType<PlayerController>().gameObject;

    private void Update()
    {
        transform.position += Vector3.back * Random.Range(1.5f,4f) * Time.deltaTime;
        if (gameObject.activeSelf && (gameObject.transform.position.z + 2 < tank.transform.position.z))
        {
            //Invoke("RecycleThisOBS", 10f);
            PoolManager._instance.RecycleZombies(gameObject);
        }
    }
}