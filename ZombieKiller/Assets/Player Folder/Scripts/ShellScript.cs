using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellScript : MonoBehaviour
{
    Rigidbody Shell_RB = null;
    GameObject temp;
    AudioSource audiocource = null;
    private void Awake()
    {
        if (Shell_RB == null)
            Shell_RB = GetComponent<Rigidbody>();
        audiocource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (transform.position.z - FindObjectOfType<PlayerController>().gameObject.transform.position.z >= 150f)
        {
            RemoveShell();
        }
    }
    public void FireShell()
    {
        Shell_RB.AddForce(Vector3.forward.normalized * 4000f);
        //Invoke("RemoveShell", 5f);
    }

    void RemoveShell()
    {
        //this.gameObject.SetActive(false);
        Shell_RB.velocity = Vector3.zero;
        PoolManager._instance.RecylceShells(gameObject);
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.tag == "Enemy")
    //        gameObject.SetActive(false);
    //}

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Obstacle"))
        {
            Points_Data._instance.ScoreObsactleShoot();
            other.gameObject.SetActive(false);
            //print("BOOM");
            audiocource.clip = Audio_Manager._instance.ObstacleBlast;
            audiocource.Play();
            PoolManager._instance.Blast.transform.position = other.transform.position;
            PoolManager._instance.Blast.transform.rotation = other.transform.rotation;
            PoolManager._instance.Blast.Play();
            Invoke("DelayedShellRemoval", .2f);
        }
        else if (other.CompareTag("Zombie"))
        {
            Points_Data._instance.ScoreZombieShoot();
            other.gameObject.SetActive(false);
            audiocource.clip = Audio_Manager._instance.ZombieBlast;
            audiocource.Play();
            PoolManager._instance.Blast.transform.position = other.transform.position;
            PoolManager._instance.Blast.transform.rotation = other.transform.rotation;
            PoolManager._instance.Blast.Play();
            //Invoke("DelayedShellRemoval",.2f);
        }
    }

    void DelayedShellRemoval() => RemoveShell();
}
