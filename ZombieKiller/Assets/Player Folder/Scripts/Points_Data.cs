using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Points_Data : MonoBehaviour
{
    [SerializeField] public static int Base_ScoreZ = 5, Base_ScoreO = 9, Zombie_Strek = 10, temp;
    [SerializeField] int Extra_Point = 1, Extra_Streak = 1, Multpliyer = 1;
    static long Score_int = 0000;
    //UI
    [SerializeField] Text Score = null, Multipley_text = null, StreakInfo = null;

    //Instance
    public static Points_Data _instance = null;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (!GameManager.GameOver)
        {
            if (temp <= 0)
            {
                Multpliyer++;
                Zombie_Strek += Extra_Streak;
                temp = Zombie_Strek;
            }
            Score.text = Score_int.ToString();
            Multipley_text.text = "x" + Multpliyer;
            StreakInfo.text = temp + " ZOMBIES REMAINING";
        }
    }

    private void Start()
    {
        temp = Zombie_Strek;
        Score.text = Score_int.ToString();
        Multipley_text.text = "x" + Multpliyer;
        StreakInfo.text = temp + " ZOMBIES REMAINING";
    }

    public void ScoreZombieShoot() {
        Score_int += ((Base_ScoreZ + Extra_Point) * Multpliyer);
        temp--;
    }

    public void ScoreZombieCollision() {
        Score_int += (Base_ScoreZ * Multpliyer);
        temp--;
    }

    public void ScoreObsactleShoot() => Score_int += (Base_ScoreO * Multpliyer);

    public void ResetMultiplyer() => Multpliyer = 1;

    public void ResetEverything()
    {
        ResetMultiplyer();
        Score_int = 0;
        Zombie_Strek = 10;
    }
}
