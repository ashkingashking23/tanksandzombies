using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour
{
    #region Variables
 
    public Vector3 speed;
    public float xSpeed = 8f, zSpeed = 15f, acceleration = 15f, deccelaration = 10f;
    public float lowSoundPitch, normalSOundPitch, highSoundPitch;
    protected float rotation_speed = 10f, maximum_rotation_angle = 10f;
    [SerializeField] public AudioClip engineOnSound, engineOffSound;
    bool is_slow;
    AudioSource soundManager = null;
    #endregion

    #region Callbacks
    private void Awake()
    {
        soundManager = GetComponent<AudioSource>();
        speed = new Vector3(0f, 0f, zSpeed);
    }

    #endregion

    #region Methods
    protected void MoveLeft()
    {
        speed = new Vector3(-xSpeed / 2f, 0f, speed.z);
    }

    protected void MoveRight()
    {
        speed = new Vector3(xSpeed / 2f, 0f, speed.z);
    }

    protected void MoveStraight()
    {
        speed = new Vector3(0f, 0f, speed.z);
    }

    protected void MoveNormal()
    {
        if (is_slow)
        {
            is_slow = false;
            soundManager.Stop();
            soundManager.clip = engineOnSound;
            soundManager.volume = 0.5f;
            soundManager.Play();
        }
        speed = new Vector3(speed.x, 0f, zSpeed);
    }

    protected void MoveSlow()
    {
        if (!is_slow)
        {
            is_slow = true;
            soundManager.Stop();
            soundManager.clip = engineOffSound;
            soundManager.volume = 0.8f;
            soundManager.Play();
        }
        speed = new Vector3(speed.x, 0f, deccelaration);
    }

    protected void MoveFast() => speed = new Vector3(speed.x, 0f, acceleration);

    #endregion
}
