using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Experimental.GlobalIllumination;

public class GameManager : MonoBehaviour
{
    static GameManager _instance = null;
    public static GameManager Instance { get { return _instance; } }
    public static bool Paused = false, GameOver = false;
    [SerializeField] Transform[] lanes;
    [SerializeField] GameObject panel_GO, panel_PAUSED;
    //[SerializeField] GameObject[] obstacleObjects, zombieObjects;
    [SerializeField] float maxSpawnDelay, minSpawnDelay, halfLenght = 110f;
    BaseController baseControllerRef;
    GameObject temp;

    private void Awake()
    {
        //QualitySettings.vSyncCount = 60;
        Application.targetFrameRate = 60;
        if (_instance == null)
        {
            _instance = this;
            //DontDestroyOnLoad(this.gameObject);
            return;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        baseControllerRef = FindObjectOfType<BaseController>();
        //halfLenght = 200f;
        Time.timeScale = 1f;
        StartCoroutine("ObstaclesGenerator");
    }

    private void Update()
    {
        print(GameOver + " && " + Paused);
        //StartCoroutine("ObstaclesGenerator");
        if (Input.GetKeyDown(KeyCode.Escape) && !GameOver)
        {
            Paused = !Paused;
            panel_PAUSED.SetActive(Paused);
           
                
            //print("Game pause > " + Paused);
        }
        if (Paused)
        {
            if (Input.GetKeyDown(KeyCode.M))
                Menu();
        }
        panel_GO.SetActive(GameOver);
        Cursor.visible = GameOver;
        Time.timeScale = Paused == true ? 0f : 1f;
    }

    IEnumerator ObstaclesGenerator()
    {
        float timer = Random.Range(minSpawnDelay, maxSpawnDelay) / 2f /*/ baseControllerRef.speed.z*/;
        yield return new WaitForSeconds(timer);
        SpawnObstacles(FindObjectOfType<PlayerController>().gameObject.transform.position.z + halfLenght);
        StartCoroutine("ObstaclesGenerator");
    }

    void SpawnObstacles(float zPos)
    {
        int randommmm = Random.Range(0, 10);
        if (randommmm >= 0 && randommmm < 5)
        {
            int ObstacleLane = Random.Range(0, lanes.Length), zombieLane = 0;
            AddObstacles(new Vector3(lanes[ObstacleLane].transform.position.x, -.1f, zPos), Random.Range(0, PoolManager._instance.obstaclePrefabs.Length));
            AddMines(new Vector3(lanes[ObstacleLane].transform.position.x + Random.Range(-.2f, .2f), 0f, zPos - 2.5f));
            if (ObstacleLane == 0)
            {
                zombieLane = Random.Range(0, 3) == 1 ? 2 : 3;
            }
            else if (ObstacleLane == 1)
            {
                zombieLane = Random.Range(0, 3) == 0 ? 2 : 0;
            }
            else if (ObstacleLane == 3)
            {
                zombieLane = Random.Range(0, 3) == 2 ? 1 : 2;
            }
            AddZombies(new Vector3(lanes[zombieLane].position.x, -.1f, zPos + 2.5f));
        }
    }

    void AddObstacles(Vector3 position, int type)
    {
        temp = PoolManager._instance.spawnObstacles();
        temp.transform.position = position;
        // temp.transform.rotation = Quaternion.identity;

        bool mirror = Random.Range(0, 2) == 1;
        switch (type)
        {
            case 0:
                temp.transform.rotation = Quaternion.Euler(0f, mirror ? -20 : 20, 0f);
                break;
            case 1:
                temp.transform.rotation = Quaternion.Euler(0f, mirror ? -20 : 20, 0f);
                break;
            case 2:
                temp.transform.rotation = Quaternion.Euler(0f, mirror ? -1 : 1, 0f);
                break;
            case 3:
                temp.transform.rotation = Quaternion.Euler(0f, mirror ? -170 : 170, 0f);
                break;
            default: return;
        }
        temp.transform.position = position;
    }

    void AddZombies(Vector3 postion)
    {
        int count = Random.Range(0, 3) + 1;
        for (int i = 0; i < count; i++)
        {
            Vector3 shift = new Vector3(Random.Range(-0.5f, 0.5f), 0f, Random.Range(1f, 10f) * i);
            temp = PoolManager._instance.spawnZombies();
            temp.transform.position = postion + shift * i;
            temp.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }
    }

    void AddMines(Vector3 positionn)
    {
        GameObject temp1 = PoolManager._instance.spawnMines();
        temp1.transform.position = positionn;
        temp1.transform.rotation = Quaternion.Euler(-90f, 0f, 0f);
    }

    #region buttons
    public void Replay()
    {
        //Destroy(this.gameObject);
        FindObjectOfType<Points_Data>().ResetEverything();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        GameOver = false;
        Time.timeScale = 1f;
    }

    public void Menu()
    {
        GameOver = Paused = false;
        Points_Data pdata =  FindObjectOfType<Points_Data>();
        pdata.ResetEverything();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    #endregion
}
