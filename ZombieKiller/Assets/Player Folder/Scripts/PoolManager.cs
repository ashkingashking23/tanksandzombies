using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public static PoolManager _instance = null;
    [SerializeField] GameObject shellPrefab;
    [SerializeField] ParticleSystem explosion;
    public ParticleSystem Blast;
    [SerializeField] int countOfCopies;
    [SerializeField]public GameObject[] obstaclePrefabs, zombiePrefab,MinesPrefab;
    List<GameObject> shellList,obsList,zombieList,miesList;
    //List<ParticleSystem> explosionList;

    private void Awake()
    {
        if (_instance != this)
        {
            _instance = this;
            INit();
            
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        CreateOtherPrefabs();
    }

    private void OnDisable()
    {
        Destroy(this);
    }

    private void Update()
    {
        
    }

    void INit()
    {
        shellList = new List<GameObject>();
        obsList = new List<GameObject>(); 
        zombieList = new List<GameObject>(); 
        miesList = new List<GameObject>();
        //explosionList = new List<ParticleSystem>();
        for (int i = 0; i < countOfCopies; i++)
        {
            GameObject temp = Instantiate(shellPrefab, transform);
            temp.SetActive(false);
            shellList.Add(temp);
        }
    }

    void CreateOtherPrefabs()
    {
        for (int i = 0; i < obstaclePrefabs.Length; i++)
        {
            GameObject temp = Instantiate(obstaclePrefabs[i], transform);
            temp.SetActive(false);      //change scales
            obsList.Add(temp);
            temp = Instantiate(obstaclePrefabs[i], transform);
            temp.SetActive(false);      //change scales
            obsList.Add(temp);
            temp = Instantiate(obstaclePrefabs[i], transform);
            temp.SetActive(false);      //change scales
            obsList.Add(temp);
        }
        for (int i = 0; i < zombiePrefab.Length; i++)
        {
            GameObject temp =  Instantiate(zombiePrefab[i], transform);
            temp.SetActive(false);
            zombieList.Add(temp);
            temp = Instantiate(zombiePrefab[i], transform);
            temp.SetActive(false);
            zombieList.Add(temp);
        }
        for (int i = 0; i < MinesPrefab.Length; i++)
        {
            GameObject temp = Instantiate(MinesPrefab[i], transform);
            temp.SetActive(false);
            miesList.Add(temp);
            temp = Instantiate(MinesPrefab[i], transform);
            temp.SetActive(false);
            miesList.Add(temp);
            temp = Instantiate(MinesPrefab[i], transform);
            temp.SetActive(false);
            miesList.Add(temp);
        }
    }

    public GameObject spawnShells()
    {
        GameObject temp = null;
        for (int i = 0; i < shellList.Count; i++)
        {
            if (!shellList[i].activeSelf)
            {
                temp = shellList[i];
                temp.SetActive(true);
                break;
            }
        }
        if (temp == null)
        {
            temp = Instantiate(shellPrefab, transform);
            temp.SetActive(false);
            shellList.Add(temp);
        }
        temp.GetComponent<ShellScript>().FireShell();
        return temp;
    }

    public GameObject spawnObstacles()
    {
        GameObject temp = null;
        for (int i = 0; i < obsList.Count; i++)
        {
            if (!obsList[i].activeSelf)
            {
                temp = obsList[i];
                temp.SetActive(true);
                break;
            }
            else
                temp = null;
        }
        if (temp == null)
        {
            temp = Instantiate(obstaclePrefabs[Random.Range(0, obstaclePrefabs.Length)], transform);
            temp.SetActive(false);
            obsList.Add(temp);
        }
        return temp;
    }

    public GameObject spawnMines()
    {
        GameObject temp = null;
        for (int i = 0; i < miesList.Count; i++)
        {
            if (!miesList[i].activeSelf)
            {
                temp = miesList[i];
                temp.SetActive(true);
                break;
            }
        }
        if (temp == null)
        {
            temp = Instantiate(MinesPrefab[Random.Range(0, MinesPrefab.Length)], transform);
            temp.SetActive(false);
            miesList.Add(temp);
        }
       //print("In this ");
        return temp;
    }

    public GameObject spawnZombies()
    {
        GameObject temp = null;
        for (int i = 0; i < zombieList.Count; i++)
        {
            if (!zombieList[i].activeSelf)
            {
                temp = zombieList[i];
                temp.SetActive(true);
                break;
            }
            //else
            //    temp = null;
        }
        if (temp == null)
        {
            temp = Instantiate(zombiePrefab[Random.Range(0, zombiePrefab.Length)], transform);
            temp.SetActive(false);
            zombieList.Add(temp);
        }
        //temp.GetComponent<ShellScript>().FireShell();
        return temp;
    }
    //public ParticleSystem spawnExplosion()
    //{
    //    ParticleSystem temp = null;
    //    for (int i = 0; i < countOfCopies; i++)
    //    {
    //        if (!explosionList[i].gameObject.activeSelf)
    //        {
    //            temp = explosionList[i];
    //            temp.gameObject.SetActive(true);
    //            break;
    //        }
    //    }
    //    if (temp == null)
    //    {
    //        temp = Instantiate(explosion, transform);
    //        temp.gameObject.SetActive(false);
    //        explosionList.Add(temp);
    //    }
    //    return temp;
    //}

    public bool RecylceShells(GameObject temp)
    {
        if (shellList.Contains(temp))
        {
            temp.SetActive(false);
            temp.GetComponent<Rigidbody>().velocity = Vector3.zero;
            return true;
        }
        else
        return false;
    }

    public void RecycleMines(GameObject temp)
    {
        if (miesList.Contains(temp))
            temp.SetActive(false);
    }
    public void RecycleObstacles(GameObject temp)
    {
        if (obsList.Contains(temp))
            temp.SetActive(false);
    }

    public void RecycleZombies(GameObject temp)
    {
        if(zombieList.Contains(temp))
            temp.SetActive(false);
    }
    //public bool RecylceExplosions(ParticleSystem temp)
    //{
    //    if (explosionList.Contains(temp))
    //    {
    //        temp.gameObject.SetActive(false);
    //        return true;
    //    }
    //    else
    //        return false;
    //}
}
