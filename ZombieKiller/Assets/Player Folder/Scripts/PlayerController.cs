using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : BaseController
{
    private Rigidbody rigidBody;
    [SerializeField]Transform Turret = null,shellStartpoint;
    [SerializeField]ParticleSystem ShootFX;
    [SerializeField]Light lightref;
    [SerializeField] AudioSource muzzleAudiosource,bodyAudiosource;
    Color initialcolor;
    bool CollidingWalls = false,movingSlow = false;
    static float counter = 0f;
    AudioSource audioSource = null;
    //--------------------------------------------------//

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        bodyAudiosource.clip = null;
        initialcolor = lightref.color;
        audioSource.clip = Audio_Manager._instance.TankMovingH;
        audioSource.Play();
    }


    private void Update()
    {
        if (!GameManager.Paused && !GameManager.GameOver)
        {
            //print(CollidingWalls);
            counter += Time.deltaTime;
            ControlUsingKeyboard();
            Turret.rotation = Quaternion.Euler(0f, 180f, 0f);
            #region ShootMechanism
            if (counter >= .4f && (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Space)))
            {
                counter = 0f;
                muzzleAudiosource.clip = Audio_Manager._instance.FireShell;
                muzzleAudiosource.Play();

                ShootMech();
                lightref.intensity = 10f;
            }
            else
                lightref.intensity = 1f;
            #endregion
            #region Tank Movement Audio
            audioSource.clip = movingSlow ? audioSource.clip = Audio_Manager._instance.TankMovingL : audioSource.clip = Audio_Manager._instance.TankMovingH;
            if (!audioSource.isPlaying)
                audioSource.Play();
            #endregion
        }
        else {
            audioSource.Stop();
        }
    }

    void FixedUpdate()
    {
        if (!GameManager.GameOver)
        {
            ChangeRotation();   //for some reason this needs to be called first :eyes:
            MoveTank();
        }
    }

    void MoveTank()
    {
        rigidBody.MovePosition(rigidBody.position + speed * Time.deltaTime);
    }

    void ControlUsingKeyboard()
    {
        if ((Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) /*&& !CollidingWalls*/)
        {
            MoveLeft();
        }
        else if ((Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) /*&& !CollidingWalls*/)
        {
            MoveRight();
        }
        else if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            //    MoveFast();
            MoveNormal();
            movingSlow = false;
        }
        else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            MoveSlow();
            movingSlow = true;
        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.D))
        {
            MoveStraight();
        }
        else if (/*Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.W) || */Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.S))
        {
            MoveNormal();
            movingSlow = false;
        }
    }

    void ChangeRotation()
    {
        if (!CollidingWalls)
        {
            if (speed.x > 0)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, maximum_rotation_angle, 0f), Time.deltaTime * rotation_speed);
            }
            else if (speed.x < 0)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, -maximum_rotation_angle, 0f), Time.deltaTime * rotation_speed);
            }
            else
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, 0f, 0f), Time.deltaTime * rotation_speed);
            }
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, 0f, 0f), Time.deltaTime * rotation_speed);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        string CollidedObstacle = " ";
        if (other != null)
        {
            CollidedObstacle = other.tag.ToString();
            switch (CollidedObstacle)
            {
                case "Zombie": other.gameObject.SetActive(false);
                    Points_Data._instance.ScoreZombieCollision();
                    bodyAudiosource.clip = Audio_Manager._instance.CollionsZombie;
                    bodyAudiosource.Play();
                    break;
                case "Obstacle": other.gameObject.SetActive(false);
                    Points_Data._instance.ResetMultiplyer();
                    bodyAudiosource.clip = Audio_Manager._instance.CollisonObstacle;
                    bodyAudiosource.Play();
                    MoveSlow();
                    movingSlow = true;
                    break;
                case "Enemy": GameManager.GameOver = true;
                    PoolManager._instance.Blast.transform.position = other.transform.position;
                    PoolManager._instance.Blast.transform.rotation = other.transform.rotation;
                    PoolManager._instance.Blast.Play();
                    audioSource.Stop();
                    print("mine!!!");
                    break;
                default: bodyAudiosource.clip = null;
                    return;
            }
           // print(CollidedObstacle);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.transform.CompareTag("Boundary"))
        {
            CollidingWalls = true;
        }// transform.rotation = Quaternion.identity;
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.transform.CompareTag("Boundary"))
            CollidingWalls = false;
    }

    public void ShootMech()
    {
        GameObject temp = PoolManager._instance.spawnShells();
        temp.transform.position = shellStartpoint.position;
        //ParticleSystem temp1 = PoolManager._instance.spawnExplosion();
        //temp.transform.rotation = shellStartpoint.rotation;
        //temp1.transform.position = shellStartpoint.position;
        //temp1.transform.rotation = shellStartpoint.rotation;
        ShootFX.Play();
    }
}



























