using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio_Manager : MonoBehaviour
{
    public static Audio_Manager _instance = null;
    [SerializeField] public AudioClip BGM,FireShell,Blast,CollionsZombie,CollisonObstacle,TankMovingH,TankMovingL,ZombieBlast,ObstacleBlast;
    AudioSource audiosource = null;
    private void Awake()
    {
        Singleton();
        if (audiosource == null)
            audiosource = GetComponent<AudioSource>();
        audiosource.clip = BGM;
        audiosource.Play();
    }

    void Singleton()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
