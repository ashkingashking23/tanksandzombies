using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollow : MonoBehaviour
{
    public Transform target;
    [SerializeField] float dist = 6.4f, height = 3.5f, HeightDamping = 3.25f, rotationDamping = .27f/*, xCurrent = 0.0f, xMax = 0.0f, xMin = 0.0f*/;
    float wantedHeight = 0f, wantedRoatationAngle = 0f,currentRotationAngle = 0f,currentHeight = 0f,temp = 0f;
    Quaternion currentRotation = Quaternion.identity;
    private void Awake()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    //private void Start()
    //{
    ////    xCurrent = Mathf.Round(xCurrent * 10f) * 0.1f;
    ////    xMax = Mathf.Round(xMax * 10f) * .1f;
    ////    xMin = Mathf.Round(xMin * 10f) * .1f;
    ////    temp = Mathf.Round(xCurrent * 10f)*.1f;
    ////    temp += Time.deltaTime * .1f;
    //}

    private void LateUpdate()
    {
        FollowPlayer();
    }



    void FollowPlayer()
    {
        wantedRoatationAngle = target.eulerAngles.y;
        wantedHeight = target.position.y + height;
        currentHeight = transform.position.y;
        currentRotationAngle = transform.eulerAngles.y;

        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRoatationAngle, rotationDamping * Time.deltaTime);
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, HeightDamping * Time.deltaTime);

        currentRotation = Quaternion.Euler(0f, currentRotationAngle, 0f);
        transform.position = target.position;
        transform.position -= currentRotation * Vector3.forward * dist;
        transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);
    }
}//class
